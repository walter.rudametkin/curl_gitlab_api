#!/bin/sh

#####################################
#         USER CONFIGURATION        #
#####################################

# NOTE: IF EMPTY, ITEMS WILL BE READ FROM THE COMMAND LINE

# Univ Lille username (e.g., marie.curie)
USERNAME=""
PASSWORD=""

# Github repository (e.g. walter.rudametkin/curl_gitlab_api)
GITLAB_REPOSITORY=""

# Server (only verified to work for gitlab.univ-lille.fr)
GITLAB_SERVER=https://gitlab.univ-lille.fr

# Users to add to repository, separated by spaces
# E.g., "marie.curie walter.rudametkin francois.boulier olivier.caron"
USERS_TO_ADD="walter.rudametkin francois.boulier olivier.caron"
#####################################


#####################################
#         DESCRIPTION               #
#####################################

function TITLE {
    printf "\n"
    printf "************************************************\n" ;
    printf "`tput bold; tput setaf 7`"
    printf "  $1\n" ;
    printf "`tput sgr0`"
    printf "************************************************\n" ;
}

TITLE "Gitlab Curl Add Users Script for UnivLille"
printf "This script adds a list of users to a gitlab repository through the web interface.\n"
printf "The gitlab server is authenticated through a Single-Sign-On Shibboleth/SAML server.\n"
printf "Current configuration works specifically for https://gitlab.univ-lille.fr\n"


###################
# SUPPORT FUNCTIONS
###################

# Colors
red=`tput setaf 1`
green=`tput setaf 2`
green_bold=`tput bold; tput setaf 2`
white=`tput bold; tput setaf 7`
reset=`tput sgr0`

# Convert text to URL encoding
rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
    c=${string:$pos:1}
    case "$c" in
      [-_.~a-zA-Z0-9] ) o="${c}" ;;
      * )               printf -v o '%%%02x' "'$c"
    esac
    encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

function cleanup {
  # Clean Temporary files
  printf "  Cleaning up temporary files... $COOKIE_JAR, $HEADER_DUMP, $PAGE..."
  rm -f $COOKIE_JAR
  rm -f $HEADER_DUMP
  rm -f $PAGE
  printf "  Done\n"
}

function quit {
  if [ $1 -eq 1 ] ; then
    printf "${red}"
    printf "\n"
    printf "================\n" ;
    printf "=     ERROR    =\n" ;
    printf "================\n" ;
    printf "${reset}"
  fi
  cleanup
  exit $1
}

###################

########################
# READ INCOMPLETE DATA #
########################

TITLE "Enter missing user and repository information"

printf "\n${white}Mandatory${reset}\n"
if [[ -z ${USERNAME} ]]; then read -p "Username: " USERNAME; else printf "Username: ${white}${USERNAME}${reset}\n"; fi
if [[ -z ${PASSWORD} ]]; then read -s -p "Password: " PASSWORD; printf "\n"; else printf "Password: ${white}xxxxx hidden xxxxx${reset}\n"; fi
if [[ -z ${GITLAB_REPOSITORY} ]]; then read -p "Repository (e.g., walter.rudametkin/curl_gitlab_api): " GITLAB_REPOSITORY; else printf "Repository: ${white}${GITLAB_REPOSITORY}${reset}\n"; fi


printf "\n${white}Optional${reset} (${green_bold}press enter to keep default values${reset})\n"
read -p "Gitlab server (default ${green}${GITLAB_SERVER}${reset}): " GITLAB_SERVER_TMP
if [[ -n ${GITLAB_SERVER_TMP} ]]; then
    GITLAB_SERVER=${GITLAB_SERVER_TMP}
fi
read -p "Users to add (default ${green}${USERS_TO_ADD}${reset}): " USERS_TO_ADD_TMP
if [[ -n ${USERS_TO_ADD_TMP} ]]; then
    USERS_TO_ADD=${USERS_TO_ADD_TMP}
fi

printf "\n"
TITLE "Verify User Configuration"
#printf "\n\n${white}Current setup is as follows${reset}\n"
printf "\tUsername          : ${green}${USERNAME}${reset}\n"
printf "\tPassword          : ${green}xxxxx hidden xxxxx${reset}\n"
printf "\tGitlab repository : ${green}${GITLAB_REPOSITORY}${reset}\n"
printf "\tGitlab server     : ${green}${GITLAB_SERVER}${reset}\n"
printf "\tUsers to add      : ${green}${USERS_TO_ADD}${reset}\n"
printf "\n${white}"
read -n 1 -s -r -p "Press any key to continue, ctrl+c to cancel  "
printf "${reset}\n\n"
#####################################


########################
# SCRIPT CONFIGURATION #
########################

DEST=${GITLAB_SERVER}/users/sign_in
#SP=${GITLAB_SERVER}/Shibboleth.sso/SAML2/POST
SP=${GITLAB_SERVER}/Shibboleth.sso/Login
TARGET=${GITLAB_SERVER}/users/auth/shibboleth/callback
IDP=https://idp.univ-lille.fr/idp/shibboleth
WAYF=https://wayf.univ-lille.fr/WAYF.php
CAS=https://cas.univ-lille.fr/login
TOKEN_NAME=script-generated

#TODO : Configure expiration date for very short expiration!
TOKEN_EXPIRATION_DATE="" #Format is : 
#Access level for all users added
ACCESS_LEVEL=40

###########
# PREPARE #
###########

# URL encode variables
USERNAME=$(rawurlencode "$USERNAME")
PASSWORD=$(rawurlencode "$PASSWORD")

GITLAB_REPOSITORY_enc=$(rawurlencode "$GITLAB_REPOSITORY")
GITLAB_SERVER_enc=$(rawurlencode "${GITLAB_SERVER}")
SP_enc=$(rawurlencode "${SP}")
IDP_enc=$(rawurlencode "${IDP}")
WAYF_enc=$(rawurlencode "${WAYF}")
TARGET_enc=$(rawurlencode "${TARGET}")

#Temporary files
COOKIE_JAR=.cookie_jar
HEADER_DUMP=.headers
PAGE=.output.html


##################
# AUTHENTICATION #
##################
TITLE "AUTHENTICATION PROCESS"

#The script itself is below

# first: store cookies from the page that will display the WAYF
#curl -L -k -c $COOKIE_JAR "$DEST" -w %{url_effective} -o /dev/null
#curl -L -H $UA2 -c $COOKIE_JAR -D $HEADER_DUMP "$DEST" -w %{url_effective} -o /dev/null -s
#curl -L -c .cookieJar  https://gitlab.univ-lille.fr/users/sign_in -w %{url_effective} -D .headers
curl -L -c $COOKIE_JAR -D $HEADER_DUMP "$DEST" -o /dev/null -s #-w %{url_effective}

# second: select univ lille and permanent option
#$WAYF?entityID=${GITLAB_SERVER_enc}&return=${SP_enc}?SAMLDS=1&target=${TARGET_enc}' --data 'user_idp=${IDP_enc}&Select=Select&session=true&permanent=100'
#curl -L 'https://wayf.univ-lille.fr/WAYF.php?entityID=https%3A%2F%2Fgitlab.univ-lille.fr&return=https%3A%2F%2Fgitlab.univ-lille.fr%2FShibboleth.sso%2FLogin%3FSAMLDS%3D1%26target%3Dhttps%253A%252F%252Fgitlab.univ-lille.fr%252Fusers%252Fauth%252Fshibboleth%252Fcallback' --data 'user_idp=https%3A%2F%2Fidp.univ-lille.fr%2Fidp%2Fshibboleth&Select=Select&session=true&permanent=100' -b .cookieJar -c .cookieJar -D .headers  -w %{url_effective} -o /dev/null
#curl -L "$WAYF?entityID=$(rawurlencode ${GITLAB_SERVER}\&return=${SP}?SAMLDS=1\&target=${TARGET})" --data "user_idp=$(rawurlencode \"${IDP}&Select=Select&session=true&permanent=100\")" -b .cookieJar -c .cookieJar -D .headers  -w %{url_effective} -o /dev/null
curl -L "$WAYF?entityID=${GITLAB_SERVER_enc}&return=$(rawurlencode ${SP}?SAMLDS=1\&target=${TARGET_enc})" --data "user_idp=${IDP_enc}&Select=Select&session=true&permanent=100" -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP -o /dev/null -s #-w %{url_effective}


# third: save and continue
#curl -L 'https://wayf.univ-lille.fr/WAYF.php?entityID=https%3A%2F%2Fgitlab.univ-lille.fr&return=https%3A%2F%2Fgitlab.univ-lille.fr%2FShibboleth.sso%2FLogin%3FSAMLDS%3D1%26target%3Dhttps%253A%252F%252Fgitlab.univ-lille.fr%252Fusers%252Fauth%252Fshibboleth%252Fcallback' --data 'permanent_user_idp=https%3A%2F%2Fidp.univ-lille.fr%2Fidp%2Fshibboleth&Select=Save+and+continue' -b .cookieJar -c .cookieJar -D .headers  -w %{url_effective} --output $PAGE
curl -L "$WAYF?entityID=${GITLAB_SERVER_enc}&return=$(rawurlencode ${SP}?SAMLDS=1\&target=${TARGET_enc})" --data "permanent_user_idp=${IDP_enc}&Select=Save+and+continue" -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP --output $PAGE -s #-w %{url_effective}

CAS_ID=$( cat $PAGE | grep 'name="execution"' | sed 's/.*value="//' | sed 's/" \/>//'  )

if [[ -z ${CAS_ID} ]]; then
  echo "ERROR, script is broken ! CAS_ID not obtained'";
  quit 1
fi
echo "CAS_ID obtained, proceeding to authentication";


# fourth: login
curl -L "$CAS?service=$(rawurlencode ${IDP}/idp/Authn/ExtCas?conversation=e1s1)&entityId=${GITLAB_SERVER_enc}" \
  --data "username=$USERNAME&password=$PASSWORD&lt=&execution=$CAS_ID&_eventId=submit" \
  -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP --output $PAGE -s #-w %{url_effective}

SAML_RESPONSE=$(grep 'name=.SAMLResponse' $PAGE | sed 's/.*value..//' | sed 's/\".*//')
if [[ -z ${SAML_RESPONSE}  ]]; then
  echo "ERROR, authentication failed, please verify credentials and configuration";
  quit 1
fi
echo "SAML_RESPONSE obtained";


# fifth: redirect to continue
relaystate=`grep 'name=.RelayState' $PAGE | sed 's/.*value..//' | sed 's/\".*//' | perl -MHTML::Entities -le 'while(<>) {print decode_entities($_);}'` ;
printf "Relay state: $relaystate\n"
target=$(grep 'form action.' $PAGE | sed 's/.*action.//' | sed 's/\"//' | sed 's/".*//' | perl -MHTML::Entities -le 'while(<>) {print decode_entities($_);}') ;
printf "Target $target\n"

# Post the form and get redirected to the page
#curl -L --data "RelayState=$relaystate" --data-urlencode SAMLResponse@samlresponse.txt -i -b $COOKIE_JAR -c $COOKIE_JAR "$target"
curl -L -i -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP --data "RelayState=$relaystate" --data-urlencode SAMLResponse=$SAML_RESPONSE "$target" -s -o /dev/null #-o $PAGE -w %{url_effective}

################
# ADDING TOKEN #
################

body_header=$(curl -s -L -i -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP "${GITLAB_SERVER}/profile/personal_access_tokens") #-o $PAGE -w %{url_effective}
csrf_token=$(echo $body_header | perl -ne 'print "$1\n" if /authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

if [[ -z ${csrf_token} ]]; then
  echo "ERROR, csrf_token not obtained from gitlab, check html output for information'";
  echo
  echo $body_header
  quit 1
fi
echo "csrf_token obtained from gitlab page";

printf "so far so good... authentication looks ok\n"


#body_header=$(curl -L "https://gitlab.univ-lille.fr/profile/personal_access_tokens" \
#--data 'personal_access_token[name]=script-generated&personal_access_token[expires_at]=&personal_access_token[scopes][]=api' -i -b .cookieJar -c .cookieJar -D .headers --data-urlencode "authenticity_token=${csrf_token_old2}") #WORKS!!!

body_header=$(curl -s -L -i -b $COOKIE_JAR -c $COOKIE_JAR -D $HEADER_DUMP "${GITLAB_SERVER}/profile/personal_access_tokens" \
  --data "personal_access_token[name]=$TOKEN_NAME&personal_access_token[expires_at]=$TOKEN_EXPIRATION_DATE&personal_access_token[scopes][]=api" \
  --data-urlencode "authenticity_token=${csrf_token}") #-o $PAGE -w %{url_effective}

TOKEN_VALUE=$(echo $body_header | perl -ne 'print "$1\n" if /created-personal-access-token"[[:blank:]]value="(.+?)"/' | sed -n 1p)

TITLE "ADDING NEW TOKEN"
echo "URL             : ${GITLAB_SERVER}/profile/personal_access_tokens"
echo "Token name      : $TOKEN_NAME"
echo "Token value     : $TOKEN_VALUE  <---  SAVE"
if [[ -z ${TOKEN_EXPIRATION_DATE} ]]; then
  echo "Expiration date : NEVER"
else
  echo "Expiration date : $TOKEN_EXPIRATION_DATE"
fi


TITLE "ADDING NEW USERS"

BASE_CURL_CMD="curl -s -L -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $TOKEN_VALUE' ${GITLAB_SERVER}/api/v4"
PROJECT_ID=$(eval ${BASE_CURL_CMD}/projects/${GITLAB_REPOSITORY_enc} -XGET | python -c 'import sys, json; print(json.load(sys.stdin)["id"])')

if [[ -z ${PROJECT_ID} ]]; then
  echo "ERROR, Project ID not obtained... quitting'";
  quit 1
fi
echo "Project ID     : $PROJECT_ID"
echo "Project Name   : ${GITLAB_REPOSITORY}"
echo "Repository URL : ${GITLAB_SERVER}/${GITLAB_REPOSITORY}"
echo "Current members"
echo $(eval ${BASE_CURL_CMD}/projects/${PROJECT_ID}/members/all -XGET) | python -m json.tool | grep username | sed 's/"//g' | sed 's/,$//'


#Convert users to array
USERS_TO_ADD_ARRAY=($USERS_TO_ADD)

printf "\nAdding${white} ${#USERS_TO_ADD[@]} ${reset}users\n"

# For all users
for ((i = 0; i < ${#USERS_TO_ADD_ARRAY[@]}; i++))
do
  USERNAME="${USERS_TO_ADD_ARRAY[$i]}"

  # Get user ID
  printf "\tAdding ${white}${USERNAME}${reset} with ID: "
  OUTPUT=$(eval ${BASE_CURL_CMD}/users?username=$USERNAME -XGET)
  if [[ "${OUTPUT}" == "[]" ]]; then
    # Fail
    printf "${red} ERROR: ID for ${USERNAME} not found${reset}"
  else
    # Add user to repository
    USER_ID=$(echo $OUTPUT | python -c 'import sys, json; print(json.load(sys.stdin)[0]["id"])')
    printf "${green}${USER_ID} "

    OUTPUT=$(curl -s -L --header "PRIVATE-TOKEN: $TOKEN_VALUE" ${GITLAB_SERVER}/api/v4/projects/$PROJECT_ID/members -XPOST --data "user_id=${USER_ID}&access_level=${ACCESS_LEVEL}" | python -m json.tool | grep message | sed 's/"//g' | sed -e 's/^[ \t]*//')

    if [[ -z "${OUTPUT}" ]]; then
      printf " SUCCESS"
    else
      printf " ${OUTPUT}"
    fi
    printf "${reset}"

    # Instructions to delete this user
    #printf "\n\tYou can delete $USERNAME with:\n"
    #printf "\tcurl -s -L --header \"PRIVATE-TOKEN: $TOKEN_VALUE\" ${GITLAB_SERVER}/api/v4/projects/$PROJECT_ID/members/${USER_ID} -XDELETE\n\n"
  fi
  printf "\n"
done

printf "\nYou can delete users from your repository with:\n"
printf "curl -s -L --header \"PRIVATE-TOKEN: $TOKEN_VALUE\" ${GITLAB_SERVER}/api/v4/projects/$PROJECT_ID/members/USER_ID -XDELETE\n\n"

quit 0
