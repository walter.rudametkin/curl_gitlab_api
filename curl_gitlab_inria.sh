#!/bin/bash

rawurlencode() {
  local string="${1}"
  local strlen=${#string}
  local encoded=""
  local pos c o

  for (( pos=0 ; pos<strlen ; pos++ )); do
     c=${string:$pos:1}
     case "$c" in
        [-_.~a-zA-Z0-9] ) o="${c}" ;;
        * )               printf -v o '%%%02x' "'$c"
     esac
     encoded+="${o}"
  done
  echo "${encoded}"    # You can either set a return variable (FASTER) 
  REPLY="${encoded}"   #+or echo the result (EASIER)... or both... :p
}

UA2="-H 'authority: gitlab.inria.fr' \
  -H 'cache-control: max-age=0' \
  -H 'origin: https://gitlab.inria.fr' \
  -H 'upgrade-insecure-requests: 1' \
  -H 'dnt: 1' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -H 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.7 Safari/537.36' \
  -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
  -H 'sec-fetch-site: same-origin' \
  -H 'sec-fetch-mode: navigate' \
  -H 'sec-fetch-user: ?1' \
  -H 'sec-fetch-dest: document' \
  -H 'accept-language: en-US,en;q=0.9,es;q=0.8,fr;q=0.7' \
  --compressed "

gitlab_host="https://gitlab.inria.fr"
gitlab_repository="wrudamet/test_curl_api"
gitlab_user=""
#FILL IN HERE:
gitlab_password_raw=""
gitlab_password=$(rawurlencode "$gitlab_password_raw")
gitlab_repository_uri=$(rawurlencode "$gitlab_repository")

echo "1:$gitlab_password_raw 2:$gitlab_password"
echo "1:$gitlab_repository 2:$gitlab_repository_uri"

#exit 0

body_header=$(curl -L -H $UA2 -c cookies.txt -i https://gitlab.inria.fr/users/sign_in -s)

csrf_token_old=$(echo $body_header | perl -ne 'print "$1\n" if /new_user.*?authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)
csrf_token_old2=$(echo $body_header | perl -ne 'print "$1\n" if /authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)
csrf_token="$(echo $body_header | tidy -q 2>/dev/null | rg -C2 csrf-token | rg -v csrf-token | sed 's/^"\(.*\)".*/\1/')"
echo $csrf_token

echo $body_header | tidy -q 2>/dev/null | rg -C2 token ; echo "****** extracted values ******" ;
echo "New*$csrf_token*"
echo "Old*$csrf_token_old*"
echo "Old2*$csrf_token_old2*"

#Login
printf "\n\n*************************\n"
printf "Login"
printf "\n*************************\n\n"

curl -L -H '$UA2' -b cookies.txt -c cookies.txt -i "${gitlab_host}/users/sign_in" \
	--data "user[login]=${gitlab_user}&user[password]=${gitlab_password}" \
	--data-urlencode "authenticity_token=${csrf_token_old}"


#body_header=$(curl -L -H $UA2 -b cookies.txt -i "${gitlab_host}/profile/personal_access_tokens" -s)

#sleep 3

printf "\n\n*************************\n"
printf "Token Page"
printf "\n*************************\n\n"

body_header=$(curl -L -H '$UA2' -b cookies.txt -i "${gitlab_host}/profile/personal_access_tokens" -s)

csrf_token_old=$( echo $body_header | perl -ne 'print "$1\n" if /new_user.*?authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)
csrf_token_old2=$(echo $body_header | perl -ne 'print "$1\n" if /authenticity_token"[[:blank:]]value="(.+?)"/' | sed -n 1p)
csrf_token="$(echo $body_header | tidy -q 2>/dev/null | rg -C2 csrf-token | rg -v csrf-token | sed 's/^"\(.*\)".*/\1/')"
echo $csrf_token

echo $body_header | tidy -q 2>/dev/null | rg -C2 authenticity_token ; echo "****** extracted values ******" ;
echo "New*$csrf_token*"
echo "Old*$csrf_token_old*"
echo "Old2*$csrf_token_old2*"

#exit 1;

# 4. curl POST request to send the "generate personal access token form"
#      the response will be a redirect, so we have to follow using `-L`
body_header=$(curl -L -H '$UA' -b cookies.txt "${gitlab_host}/profile/personal_access_tokens" \
	--data-urlencode "authenticity_token=${csrf_token_old2}" \
	--data 'personal_access_token[name]=golab-generated&personal_access_token[expires_at]=&personal_access_token[scopes][]=api')

# 5. Scrape the personal access token from the response HTML
personal_access_token=$(echo $body_header | perl -ne 'print "$1\n" if /created-personal-access-token"[[:blank:]]value="(.+?)"/' | sed -n 1p)


if [[ -z "$personal_access_token" ]]; then
  echo "ERROR: Personal Access Token was not obtained"
  exit 1
else
  printf "\n\n*************************\n"
  printf     "Personal Access Token: $personal_access_token"
  printf   "\n*************************\n\n"
fi


curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${gitlab_repository_uri}"
project_id="$(eval ${curl_cmd} | python -c 'import sys, json; print(json.load(sys.stdin)["id"])' )"

if [[ -z "${project_id}" ]]; then
  echo "ERROR: Project ID not found, check project name and urls... exiting"
  exit 1
else
  #echo "SUCCESS: Project ID was found"
  printf "\n\n*************************\n"
  printf     "Project id: ${project_id}"
  printf   "\n*************************\n\n"
fi

printf "\n\n*************************\n"
printf     "Members ${gitlab_repository}"
printf   "\n*************************\n\n"
curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${project_id}/members/all"
#curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${gitlab_repository_uri}"
eval ${curl_cmd} | python -m json.tool

printf "\n\n*************************\n"
printf     "New Members"
printf   "\n*************************\n\n"


#Print a member
curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/users?username=${gitlab_user}"
eval ${curl_cmd} | python -m json.tool
curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: Fhyj5svwpQP1xwjDx8aK' https://gitlab.inria.fr/api/v4/users?username=wrudamet


#Add a member
curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/users?username=${gitlab_user}"
eval ${curl_cmd} | python -m json.tool
curl -L -XPOST --header 'PRIVATE-TOKEN: Fhyj5svwpQP1xwjDx8aK' --data "user_id=10058&access_level=40" https://gitlab.inria.fr/api/v4/projects/${project_id}/members | python -m json.tool

printf "\n\n*************************\n"
printf     "Members ${gitlab_repository}"
printf   "\n*************************\n\n"
curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${project_id}/members/all"
#curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${gitlab_repository_uri}"
eval ${curl_cmd} | python -m json.tool
#delete member
curl -L -XDELETE --header 'PRIVATE-TOKEN: Fhyj5svwpQP1xwjDx8aK' https://gitlab.inria.fr/api/v4/projects/${project_id}/members/10058

printf "\n\n*************************\n"
printf     "Members ${gitlab_repository}"
printf   "\n*************************\n\n"
curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${project_id}/members/all"
#curl_cmd="curl -L -XGET -H 'Content-Type: application/json' --header 'PRIVATE-TOKEN: $personal_access_token' https://gitlab.inria.fr/api/v4/projects/${gitlab_repository_uri}"
eval ${curl_cmd} | python -m json.tool

echo
